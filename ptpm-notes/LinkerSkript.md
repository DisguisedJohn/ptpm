# Linker skript
- obsahuje informaci o rozložení paměťových prostorů
- rozdělení pamětí vychází z konstrukce mikroprocesoru (velikosti pamětí, umístění periferií)
- syntaxem se podobá Cčku
- umístění v paměti (jednotlivých prostorech) je sekvenční -> zasebou podle umístění v linker skriptu
- "." - aktuální adresa 
- . = ALIGN(8) - zarovnani aktualni adresy tak aby byla delitelna 8

- #Sekce
	- vstupní: sekce vstupních objektových souborů
	- výstupní: sekce ve výstupního (elf) souboru
- Bloky kódu:
	- MEMORY - rozložení paměti (adresy, rozsahy)
	- SECTIONS - závislost mezi vstupními a výstupními sekcemi  (.data, .text, .bss, ...) viz [[Linker#Práce s globálními proměnnými v rámci linkeru]]
		- obsahují pobloky
		- každý blok definuje které vstupní sekce vstupují, jaký bude výstup a v jakém pameťovém prostoru budou

![[linker-script.png]]

		
```c
MEMORY
{
	m_flash_vectab	(R) : ORIGIN = 0x00000000, LENGTH = 0x000000C0		/* 192 B */
	m_flash_gap		(R) : ORIGIN = 0x000000C0, LENGTH = 0x00000340		/* 832 B */
	m_flash_fcf		(R) : ORIGIN = 0x00000400, LENGTH = 0x00000010		/* 16 B */
	m_flash_bulk	(RX) : ORIGIN = 0x00000410, LENGTH = 0x0001FBF0		/* 128 kB - 192 B - 832 B - 16 B */
	m_sram			(RW) : ORIGIN = 0x1FFFF000, LENGTH = 0x00004000		/* 16 kB */
}

SECTIONS
{
		.bss : //vystupni sekce
		{
			BSS_BEGIN = .; //Ulozeni adresy pocatku bloku pameti pro .bss
			*(.bss)		   //Vlozeni sekci ze vstupnich souboru	
			*(.bss*)	   //Vlozeni i vsech podsekci
			*(COMMON)	   //Prakticky to same .bss
			BSS_END = .;   //Ulozeni adresy konce bloku pameti pro .bss 
		} > m_sram
		
		.data : ALIGN_WITH_INPUT
		{
			DATA_BEGIN = .;
			*(.data)
			*(.data*)
			DATA_END = .;
		} > m_sram AT> m_flash_bulk //AT> inicializacni obraz (init. data) ulozit do m_flash_bulk pameti
		DATA_INIT = LOADADDR(.data) //Vrati adresu inicializacniho obrazu z m_flash_bulk
	
}
```