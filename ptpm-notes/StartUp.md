# Start-up

- po hardwarovém nastartování se prohledává začátek paměti, kde je umístěna tabulka vektorů
	- první 32 bitové číslo obsahuje adresu  **stacku**
	- druhé 32 bitové číslo obsahuje adresu první volané funkce **ResetHandler**, která se stará o volání dalších instrukcí (či funkcí) - která funkce se bude na této pozici nacházet je definováno při linkování parameter **--entry** viz [[Linker#Práce s CMD terminálem]]
	- 
	1. Jako první se volá funkce **SystemInit,** která se stará o nastavení kontroleru samotného (hodiny, periferie, atd..)
	2. dojde k přesunutí **Tabulky vektorů** z flash paměti do RAM (do paměťového bloku definováného již při linkování) - toto se dělá za účelem možnosti jednoduché změny vektorové tabulky pro možnost nastavení funkcí obsluh přerušení - přesun se skládá z memcopy dat z flash do ram a pak nastavení nové adresy do registrů <- tento úkon se děje minimálně v našich projektech
	3.  následuje memcopy všech dalších inicializačních hodnot pro **.data** a nulování datové paměti pro **.bss** 