# Toolchain
- skupina spustitelných aplikací (Kompilátor, assembler, linker, debuger, objcopy, ...) 
- dá se vytvořit vlastní pomocí [[Crosstools-NG]]

### Triplet 
- způsob pojmenovávání souborů aplikací toolchainu
- (arm-atollic-eabi-část_toolchainu) 
- cílová architektura - název výrobce/platformy - eabi 
- eabi - standard kooperace mezi programy (embedded #ABI)