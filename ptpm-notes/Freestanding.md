# Freestanding aplikace
- neběží na operačním systému, nepoužívá externí knihovny, nástroje, hlavičkové soubory, atd...
- běži na takzvaném "železe" - nejblíže hardwaru co to jen jde 
- volání #libgcc - knihovna která obsahuje low level funkce (třeba při volání dělení ho nahradí jinými funkcemi) <- pro různé procesory různé knihovny libgcc
- při použití gcc musíme k linkeru volat i libgcc
- libgcc transparentě volá funkce pro práce s pamětí 
	- (normálně jsou definované v libc, ale my libc nepoužíváme) <- memset, ...