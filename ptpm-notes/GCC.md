## GCC kompilátor
-   opensource project
-   není jenom Cčkový kompilátor

### Kompilační proces
- na začátku zdrojové soubory - source files (*.c, *.s, ...)
- Pro všechny Cčkové soubory zavolán **kompilátor** GCC, pro Skové soubory assember GAS
- Výstupem objektové soubory (*.o) - nelze poznat jestli zdroj byl assember nebo C
- Potom se volá **Linker** - výstupem *.elf <- spustitelný soubor
- *.elf se dá vložit do debuggeru (je to vlastně software) - Ozone, GDB - který komunikuje přes ladící sondu do targetu (u CM0P přes SWD - single wire debug)
- objcopy *.elf -> *.hex
- aplikace kompilátoru, assembleru, linkeru, atd ... tvoří [[Toolchain]]

### Instrukční sady
- ARM 
	- základní ortogonální instrukční sada 
	- nepoužíváme pro Cortex-M - **NEUMÍ TO**
	- ortogonální - pro všechno můžeme použít libovolné registry
	- všechny instrukce 32bit
- Thumb-2
	- 16/32bit instrukce (úspora místa při použití 16bit - code density)
	- není úplně ortogonální, tzn. ne do všech registrů můžů všechno vložit

### Práce s CMD/terminálem
```shell
arm-atollic-eabi-gcc 
-c input.c 
-mthumb
-mcpu=cortex-m0plus
-std=c89
-DMACRO=VAL
-Iinclude_path
-O0
-function-sections
-fdata-sections
-g3
-fstack-usage 
-Wall 
-ffreestanding 
-o output.o
```

**-c** - specifikace souboru pro kompilaci
**-mthumb** - použití instrukční sady Thumb(-2)
**-mcpu** - konkrétní typ procesoru, pro který bude generován strojový kód - různé architektury umí jiné množství instrukcí
**-std** - standard jazyka C
**-D**- možnost nastavení maker (-Dnazev_makra)
**-I** - include cesta (kde hledá .h soubory, nebo jiné)
**-O** - optimalizace, O0-3 - vyšší číslo znamená že bude pracovat rychleji, Os - optimalizace velikosti, Og - debugovatelný kód
**-function-sections** - souvisí se správou paměti pro globální proměnné a funkce ([[Linker]]) - pro každou funkci bude vytvořena samostatná podsekce .text (vytvoření podsekcí .text.main)

**-fdata-sections** - souvisí s pamětí (popsáno u linkeru)
**-g3** - výstupní objektový soubor bude obsahovat podporu ladění (bez toho parametru nebude třeba znát kde je původní Cčkový soubor a neumožní nám ladit po C funkci)
**-fstack-usage** - ke každému objektovému souboru vygeneruje informaci kolik místa bude zabírat v zásobníku
**-Wall** - všechny warningy jsou aktivní
**-ffreestanding** - specifikace druhu prostřední (hosting/[[Freestanding]])
**-o** - jméno výstupního souboru 

