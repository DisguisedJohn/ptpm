## Linker
- volá se jenom jednou 
- volá se spolu s knihovnou #libgcc,

## Práce s CMD/terminálem
- voláme gcc, které si samo zavolá linker pokud tam nedáme parametr -c
```bash
arm-atollic-eabi-gcc 
-mcpu=cortex-m0plus
-nostdlib 
-Wl,--defsym=SYMBOL=VAL 
-Wl,--entry=entrypoint 
-Wl,-Map=map_name.map 
-Wl,--cref 
-Wl,--gc-sections 
-Wl,--script=script_name.ld
-Wl,--oformat=elf32-littlearm 
-o mkl25z4_template.elf input1.o input2.o (...) 
-lgcc
```

**-Wl,** - volání funkcí linkeru přes Gcc (jinak by je neznal)

**-mcpu** - říkáme linkeru co to je za procesor, aby věděl kde má hledat příslušné knihovny ( #libgcc )
**-nostdlib** - linkujeme pouze explicitně knihovny uvedené na příkazovém řádku
**-defsym** - definice symbolu linkeru (obdoba define ale pro linker) s nastavením hodnoty symbolu (použití například v linker skriptu)
**--entry** - označení kde začíná první instrukce programu (adresa), definováno symbolem (např: ResetHandler), ResteHandler může obsahovat funkce prováděné před mainem a volá main samotný
**-gcsections** - garbage collector, odstraňuje nepoužité vstupní sekce (například funkce které se nepoužívají tak nejsou do výstupní .text #sekce vůbec přidány) 
**--oformat** - formát výstupního souboru
**-o** všechny vstupní soubory které chceme linkovat
**-l** - definice knihoven se kterými se linkuje (přidáva prefix lib a sufix .a <- **-lgcc = libgcc.a**)
**--script** - použitý [[LinkerSkript | linker skript]]

## Práce s globálními proměnnými  a dalšími objekty v rámci linkeru
- jelikož jednotlivé zdrojové soubory můžou sdílet globální proměnné, nemůže kompilátor do vytvořených objektů vložit specifické adresy (ostatní soubory by je neznaly), proto jsou adresy nahrazeny symboly (tato logika funguje i pro funkce)
- **symboly** jsou rozděleny do **sekcí**, kde každá #sekce má vlastní paměťový blok
- globální proměnná v C, která není inicializovaná na explicitní hodnotu, musí být 0 (výchozí hodnota)
- globální proměnné inicializované na defaultní hodnotu jsou uloženy v sekci **.bss**, explicitně inicializované na nenulovou hodnotu v sekci **.data**, kód (funkce) v sekci **.text**, const proměnné v sekci **.rodata** (read only data)
- .bss .data musí být uloženy v ramce (aby paměť snesla přepis) -> paměť musí být inicializována aby neobsahovaly žádný bordel
	- **.data** (po zlinkování) obsahuje za sebou poskládáné všechny definované globální proměnné s definovanými velikostmi - jejich incicializační hodnoty jsou za sebou poskládány v paměti flash, inicializace pomocí memcopy překopíruje inicializační hodnoty z flash na příslušnou pozici v ramce (data jednotlivých proměnných se pak nachází na relativních pozicích podle .data)
	- **.bss** obsahuje za sebou poskládáané všechny nedefinované globální proměnné, inicializace pomocí memset nastaví hodnoty v ramce na 0 (data jednotlivých proměnných se pak na relativních pozicích podle .bss)
- .rodata a .text jsou umístěny ve flash (neměnní se)