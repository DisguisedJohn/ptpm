/*
 * Name: res_alloc_io_frdm.h
 * Author: Martin Stankus
 *
 */

#ifndef _RES_ALLOC_IO_FRDM_H_
#define _RES_ALLOC_IO_FRDM_H_

#include "MKL25Z4.h"

// uart0 ***********************************************************************

#define PORT_UART0_RX				PORTA
#define IOIND_UART0_RX				1u

#define PORT_UART0_TX				PORTA
#define IOIND_UART0_TX				2u

// rgb led *********************************************************************

#define PORT_LEDR					PORTB
#define GPIO_LEDR					GPIOB
#define FGPIO_LEDR					FGPIOB
#define IOIND_LEDR					18u
#define IOMASK_LEDR					(1ul << IOIND_LEDR)
#define TPM_LEDRG					TPM2
#define TPMCH_LEDR					0u

#define PORT_LEDG					PORTB
#define GPIO_LEDG					GPIOB
#define FGPIO_LEDG					FGPIOB
#define IOIND_LEDG					19u
#define IOMASK_LEDG					(1ul << IOIND_LEDG)
#define TPMCH_LEDG					1u

#define PORT_LEDB					PORTD
#define GPIO_LEDB					GPIOD
#define FGPIO_LEDB					FGPIOD
#define IOIND_LEDB					1u
#define IOMASK_LEDB					(1ul << IOIND_LEDB)
#define TPM_LEDB					TPM0
#define TPMCH_LEDB					1u

// i2c0 accelerometer **********************************************************

#define PORT_ACC_FRDM_I2C0_SCL		PORTE
#define GPIO_ACC_FRDM_I2C0_SCL		((GPIO_Type *) 0x4000F100ul)
#define IOIND_ACC_FRDM_I2C0_SCL		24u
#define IOMASK_ACC_FRDM_I2C0_SCL	(1ul << IOIND_ACC_FRDM_I2C0_SCL)

#define PORT_ACC_FRDM_I2C0_SDA		PORTE
#define GPIO_ACC_FRDM_I2C0_SDA		((GPIO_Type *) 0x4000F100ul)
#define IOIND_ACC_FRDM_I2C0_SDA		25u
#define IOMASK_ACC_FRDM_I2C0_SDA	(1ul << IOIND_ACC_FRDM_I2C0_SDA)

#define PORT_ACC_FRDM_INT1			PORTA
#define GPIO_ACC_FRDM_INT1			GPIOA
#define FGPIO_ACC_FRDM_INT1			FGPIOA
#define IOIND_ACC_FRDM_INT1			14u
#define IOMASK_ACC_FRDM_INT1		(1ul << IOIND_ACC_FRDM_INT1)

#define PORT_ACC_FRDM_INT2			PORTA
#define GPIO_ACC_FRDM_INT2			GPIOA
#define FGPIO_ACC_FRDM_INT2			FGPIOA
#define IOIND_ACC_FRDM_INT2			15u
#define IOMASK_ACC_FRDM_INT2		(1ul << IOIND_ACC_FRDM_INT2)

#endif /* _RES_ALLOC_IO_FRDM_H_ */
