/*
 * Name: soc_def_uart.h
 * Author: Martin Stankus
 *
 */

#ifndef _SOC_DEF_UART_H_
#define _SOC_DEF_UART_H_

#define UART_SBR_115200BD_CLK48M	26u
#define UART_SBR_9600BD_CLK48M		313u

#define UART_SBR_115200BD_CLK24M	13u
#define UART_SBR_9600BD_CLK24M		156u

#endif /* _SOC_DEF_UART_H_ */
