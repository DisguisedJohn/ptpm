/*
 * Name: iic_recovery.c
 * Author: Martin Stankus
 *
 */

#include "MKL25Z4.h"

#include "bme.h"


#define OD_SET_HIGH(port, ioind)		BME_LAC1_W(&port->PDDR, ioind)
#define OD_SET_LOW(port, ioind)			BME_LAS1_W(&port->PDDR, ioind)

#define PP_SET_LOW(port, ioind)			BME_LAS1_W(&port->PDDR, ioind)

#define SCL_GET(port, ioind)			BME_UBFX_W(&port->PDIR, ioind, 1u)
#define SDA_GET(port, ioind)			BME_UBFX_W(&port->PDIR, ioind, 1u)

uint8_t iic_recovery(PORT_Type *port, GPIO_Type *gpio, uint8_t ioind_scl, uint8_t ioind_sda, uint32_t delay_scl)
{
	uint8_t rec_cnt = 0u;

	port->PCR[ioind_scl] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO);
	port->PCR[ioind_sda] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO);

	while (1) {
		rec_cnt++;

		SCL_SET_HIGH();
		delay(delay_scl);

		if(!SCL_GET()) {
			rec_cnt = 0u;
			break;
		}

		if (SDA_GET()) {
			break;
		}

		if (rec_cnt == 9u) {
			rec_cnt = 0u;
			break;
		}

		SCL_SET_LOW();
		delay(SCL_DELAY);
	}

	PORT_FRAM_SENS_I2C1_SCL->PCR[IOIND_FRAM_SENS_I2C1_SCL] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_DIS);
	PORT_FRAM_SENS_I2C1_SDA->PCR[IOIND_FRAM_SENS_I2C1_SDA] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_DIS);

	return rec_cnt;
}
