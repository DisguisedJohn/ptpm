/*
 * Name: iic_recovery.h
 * Author: Martin Stankus
 *
 */

#ifndef _IIC_RECOVERY_H_
#define _IIC_RECOVERY_H_

uint8_t iic_recovery((PORT_Type *) port, (GPIO_Type *) gpio, uint8_t ioind);

#endif /* _IIC_RECOVERY_H_ */
