/*
 * Name: iic_rd_wr.c
 * Author: Martin Stankus
 *
 */

#include "MKL25Z4.h"

#include "soc_def.h"
#include "res_alloc.h"

#include "wdog.h"
#include "delay.h"
#include "wait_bm.h"
#include "assert.h"

#define FRAM_BUS_ADDR		IIC_ADDR_FRAM_DEV0
#define FRAM_DATA_ADDR		0x6234u

#define I2C_RATE_DIV_ICR	0x11u

//t = 5us
#define SCL_DELAY			75ul

#define SCL_SET_HIGH()		(GPIO_FRAM_SENS_I2C1_SCL->PDDR &= ~IOMASK_FRAM_SENS_I2C1_SCL)
#define SCL_SET_LOW()		(GPIO_FRAM_SENS_I2C1_SCL->PDDR |= IOMASK_FRAM_SENS_I2C1_SCL)

#define SCL_GET()			(GPIO_FRAM_SENS_I2C1_SCL->PDIR & IOMASK_FRAM_SENS_I2C1_SCL)
#define SDA_GET()			(GPIO_FRAM_SENS_I2C1_SCL->PDIR & IOMASK_FRAM_SENS_I2C1_SCL)

static uint8_t iic_recovery(void);
static void iic_init(void);

static void iic_bus_start(void);
static void iic_bus_repstart(void);
static void iic_bus_stop(void);
static uint8_t iic_bus_rx(uint8_t last);
static uint8_t iic_bus_tx(uint8_t data);

static uint8_t iic_recovery(void)
{
	uint8_t rec_cnt = 0u;

	PORT_FRAM_SENS_I2C1_SCL->PCR[IOIND_FRAM_SENS_I2C1_SCL] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO);
	PORT_FRAM_SENS_I2C1_SDA->PCR[IOIND_FRAM_SENS_I2C1_SDA] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO);

	while (1) {
		rec_cnt++;

		SCL_SET_HIGH();
		delay(SCL_DELAY);

		if(!SCL_GET()) {
			rec_cnt = 0u;
			break;
		}

		if (SDA_GET()) {
			break;
		}

		if (rec_cnt == 9u) {
			rec_cnt = 0u;
			break;
		}

		SCL_SET_LOW();
		delay(SCL_DELAY);
	}

	return rec_cnt;
}

static void iic_init(void)
{
	PORT_FRAM_SENS_I2C1_SCL->PCR[IOIND_FRAM_SENS_I2C1_SCL] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_ALT6);
	PORT_FRAM_SENS_I2C1_SDA->PCR[IOIND_FRAM_SENS_I2C1_SDA] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_ALT6);

	I2C1->F = I2C_F_ICR(I2C_RATE_DIV_ICR);
}

static void iic_bus_start(void)
{
	I2C1->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK | I2C_C1_TX_MASK;
}

static void iic_bus_repstart(void)
{
	I2C1->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK | I2C_C1_TX_MASK | I2C_C1_RSTA_MASK;
}

static void iic_bus_stop(void)
{
	I2C1->C1 = I2C_C1_IICEN_MASK;
	while (I2C1->S & I2C_S_BUSY_MASK);
}

static uint8_t iic_bus_rx(uint8_t last)
{
	if (last) {
		I2C1->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK | I2C_C1_TXAK_MASK;
	} else {
		I2C1->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK;
	}

	I2C1->D;

	while (!(I2C1->S & I2C_S_IICIF_MASK));
	I2C1->S = I2C_S_IICIF_MASK;

	I2C1->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK | I2C_C1_TX_MASK;

	return I2C1->D;
}

static uint8_t iic_bus_tx(uint8_t payload)
{
	I2C1->D = payload;

	while (!(I2C1->S & I2C_S_IICIF_MASK));
	I2C1->S = I2C_S_IICIF_MASK;

	if (I2C1->S & I2C_S_RXAK_MASK) {
		return 1u;
	}

	return 0u;
}

int main(void)
{
	uint8_t data[4u];
	volatile uint8_t rec_cnt;

	wdog_init(WDOG_CONF_LPOCLK_1024_CYCLES);

	rec_cnt = iic_recovery();
	assert(rec_cnt);

	//examine the value of the rec_cnt variable
	__BKPT();

	iic_init();

	data[0u] = 0x55u;
	data[1u] = 0x00u;
	data[2u] = 0xFFu;
	data[3u] = 0xAAu;

	while (1) {

		//write transaction
		iic_bus_start();

		assert(!iic_bus_tx(IIC_MAKE_ADDR_WR(FRAM_BUS_ADDR)));

		assert(!iic_bus_tx(FRAM_DATA_ADDR >> 8u));
		assert(!iic_bus_tx((uint8_t) FRAM_DATA_ADDR));

		assert(!iic_bus_tx(data[0u]));
		assert(!iic_bus_tx(data[1u]));
		assert(!iic_bus_tx(data[2u]));
		assert(!iic_bus_tx(data[3u]));

		iic_bus_stop();

		wait_bm(WAIT_BM_250USEC_FSYS48M, 1u);

		//read transaction
		iic_bus_start();

		assert(!iic_bus_tx(IIC_MAKE_ADDR_WR(FRAM_BUS_ADDR)));

		assert(!iic_bus_tx(FRAM_DATA_ADDR >> 8u));
		assert(!iic_bus_tx((uint8_t) FRAM_DATA_ADDR));

		iic_bus_repstart();

		assert(!iic_bus_tx(IIC_MAKE_ADDR_RD(FRAM_BUS_ADDR)));

		assert(iic_bus_rx(0u) == data[0u]);
		assert(iic_bus_rx(0u) == data[1u]);
		assert(iic_bus_rx(0u) == data[2u]);
		assert(iic_bus_rx(1u) == data[3u]);

		iic_bus_stop();

		wait_bm(WAIT_BM_2MSEC_FSYS48M, 1u);

		data[0u] ^= 0xFF;
		data[1u] ^= 0xFF;
		data[2u] ^= 0xFF;
		data[3u] ^= 0xFF;
	}

	return 0;
}
