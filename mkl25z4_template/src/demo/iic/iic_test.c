/*
 * Name: iic_test.c
 * Author: Martin Stankus
 *
 */

#include "MKL25Z4.h"

#include "wdog.h"
#include "bme.h"
#include "delay.h"

#include "soc_def.h"
#include "res_alloc.h"

#define ACC_F_STATUS		0x00u
#define ACC_F_SETUP			0x09u
#define ACC_INT_SOURCE		0x0Cu
#define ACC_XYZ_DATA_CFG	0x0Eu
#define ACC_CTRL_REG1		0x2Au
#define ACC_CTRL_REG2		0x2Bu
#define ACC_CTRL_REG4		0x2Du

#define I2C_RATE_DIV_ICR	0x12u

//t = 5us
#define SCL_DELAY			75ul

#define SCL_SET_HIGH()		BME_LAC1_W(&GPIO_ACC_FRDM_I2C0_SCL->PDDR, IOIND_ACC_FRDM_I2C0_SCL)
#define SCL_SET_LOW()		BME_LAS1_W(&GPIO_ACC_FRDM_I2C0_SCL->PDDR, IOIND_ACC_FRDM_I2C0_SCL)

#define SCL_GET()			BME_UBFX_W(&GPIO_ACC_FRDM_I2C0_SCL->PDIR, IOIND_ACC_FRDM_I2C0_SCL, 1u)
#define SDA_GET()			BME_UBFX_W(&GPIO_ACC_FRDM_I2C0_SDA->PDIR, IOIND_ACC_FRDM_I2C0_SDA, 1u)

uint8_t iic_recovery(void)
{
	uint8_t rec_cnt = 0u;

	PORT_ACC_FRDM_I2C0_SCL->PCR[IOIND_ACC_FRDM_I2C0_SCL] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO);
	PORT_ACC_FRDM_I2C0_SDA->PCR[IOIND_ACC_FRDM_I2C0_SDA] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO);

	while (1) {
		rec_cnt++;

		SCL_SET_HIGH();
		delay(SCL_DELAY);

		if(!SCL_GET()) {
			rec_cnt = 0u;
			break;
		}

		if (SDA_GET()) {
			break;
		}

		if (rec_cnt == 9ul) {
			rec_cnt = 0u;
			break;
		}

		SCL_SET_LOW();
		delay(SCL_DELAY);
	}

	return rec_cnt;
}

void iic_enable(void)
{
	PORT_ACC_FRDM_I2C0_SCL->PCR[IOIND_ACC_FRDM_I2C0_SCL] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_ALT5);
	PORT_ACC_FRDM_I2C0_SDA->PCR[IOIND_ACC_FRDM_I2C0_SDA] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_ALT5);

	I2C0->F = I2C_F_ICR(I2C_RATE_DIV_ICR);
	I2C0->C1 = I2C_C1_IICEN_MASK;
}

void iic_start(void)
{
	I2C0->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK | I2C_C1_TX_MASK;
}

void iic_repstart(void)
{
	I2C0->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK | I2C_C1_TX_MASK | I2C_C1_RSTA_MASK;
}

void iic_stop(void)
{
	I2C0->C1 = I2C_C1_IICEN_MASK;
	while (BME_UBFX_B(&I2C0->S, I2C_S_BUSY_SHIFT, 1u));
}

int main(void)
{
	uint8_t rec_cnt;

	wdog_init(WDOG_CONF_DIS);

	//__BKPT();

	rec_cnt = iic_recovery();

	if (!rec_cnt) {
		__BKPT();
	}

	iic_enable();
	iic_start();

	iic_repstart();

	I2C0->D = IIC_MAKE_CTRL_RD(IIC_ADDR_ACC_FRDM);
	while (!BME_UBFX_B(&I2C0->S, I2C_S_IICIF_SHIFT, 1u));

	if (BME_UBFX_B(&I2C0->S, I2C_S_ARBL_SHIFT, 1u)) {
		__BKPT();
	}

	if (BME_UBFX_B(&I2C0->S, I2C_S_RXAK_SHIFT, 1u)) {
		__BKPT();
	}

	I2C0->S = I2C_S_IICIF_MASK;
	I2C0->C1 = I2C_C1_IICEN_MASK | I2C_C1_MST_MASK;

	while (1) {
		I2C0->D;
		while (!BME_UBFX_B(&I2C0->S, I2C_S_IICIF_SHIFT, 1u));

		if (BME_UBFX_B(&I2C0->S, I2C_S_ARBL_SHIFT, 1u)) {
			__BKPT();
		}

		I2C0->S = I2C_S_IICIF_MASK;
	}

	return 0;
}
