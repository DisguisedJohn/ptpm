/*
 * Name: support.c
 * Author: Martin Stankus
 *
 */

#include "MKL25Z4.h"

#include "wdog.h"
#include "expt.h"

#include "soc_def.h"

const uint8_t src = 0x55;
uint8_t dst;

static void __attribute__ ((interrupt)) dma_handler(void)
{
	DMA0->DMA[0u].DSR_BCR = DMA_DSR_BCR_DONE_MASK;
	DMA0->DMA[0u].DSR_BCR = DMA_DSR_BCR_BCR(1u);

	__BKPT();

	PORTA->PCR[17] = 0ul;
	PORTA->PCR[17] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO) | PORT_PCR_IRQC(PORT_PCR_IRQC_VAL_DMA_ERISE);

}

int main(void)
{
	wdog_init(WDOG_CONF_DIS);

	__BKPT();

	expt_vect_set(DMA0_IRQn, dma_handler);
	NVIC_SetPriority(DMA0_IRQn, 0u);
	NVIC_EnableIRQ(DMA0_IRQn);

	DMAMUX0->CHCFG[0u] = DMAMUX_CHCFG_ENBL_MASK | DMAMUX_CHCFG_SOURCE(DMAMUX_SRC_PORTA);

	DMA0->DMA[0u].SAR = (uint32_t) &src;
	DMA0->DMA[0u].DAR = (uint32_t) &dst;
	DMA0->DMA[0u].DSR_BCR = DMA_DSR_BCR_BCR(1u);
	DMA0->DMA[0u].DCR = DMA_DCR_EINT_MASK | DMA_DCR_ERQ_MASK | DMA_DCR_CS_MASK |
			DMA_DCR_SSIZE(1u) | DMA_DCR_DSIZE(1u);// | DMA_DCR_D_REQ_MASK;

	PORTA->PCR[17] = PORT_PCR_MUX(PORT_PCR_MUX_VAL_GPIO) | PORT_PCR_IRQC(PORT_PCR_IRQC_VAL_DMA_ERISE);

	while (1) {

	}

	return 0;
}
