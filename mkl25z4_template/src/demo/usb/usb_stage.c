/*
 * Name: usb_stage.c
 * Author: Martin Stankus
 *
 */

#include "MKL25Z4.h"
#include "wdog.h"

#include "led.h"

#include "soc_def.h"
#include "res_alloc.h"

#define USB_TRACE_LIM		128u

typedef struct {
	uint8_t istat;
	uint8_t errstat;
} USB_TRACE;

typedef struct {
	uint32_t ctrl;
	void *buff;
} USB_BDT;

volatile USB_BDT usb_bdt[8];

uint8_t buf_ep0_in0[64u];
uint8_t buf_ep0_in1[64u];

uint8_t buf_ep0_out0[64u];
uint8_t buf_ep0_out1[64u];

uint8_t buf_ep1_in0[64u];
uint8_t buf_ep1_in1[64u];

uint8_t buf_ep1_out0[64u];
uint8_t buf_ep1_out1[64u];

USB_TRACE usb_trace[USB_TRACE_LIM];
uint16_t usb_trace_ind;

static uint32_t led_state, cnt_sof, cnt_all;

void USB0_IRQHandler(void)
{
	uint8_t istat, errstat;
	cnt_all++;

	istat = USB0->ISTAT;
	errstat = USB0->ERRSTAT;

	usb_trace[usb_trace_ind].istat = istat;
	usb_trace[usb_trace_ind].errstat = errstat;

	if (istat & USB_ISTAT_SOFTOK_MASK) {
		cnt_sof++;

		if (cnt_sof == 1000) {
			cnt_sof = 0u;
			led_state = !led_state;
			led_rgb_set(LED_ID_RED, (LED_RGB_PWM_PERIOD * led_state) / 20u);
		}
	}

	USB0->ISTAT = istat;
	USB0->ERRSTAT = errstat;
}

int main(void)
{
	wdog_init(WDOG_CONF_LPOCLK_1024_CYCLES);

	led_rgb_init();

	USB0->CONTROL = USB_CONTROL_DPPULLUPNONOTG_MASK;

	USB0->BDTPAGE1;
	USB0->BDTPAGE2;
	USB0->BDTPAGE3;

	USB0->ENDPOINT[0].ENDPT = USB_ENDPT_EPRXEN_MASK | USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPHSHK_MASK;
	USB0->ENDPOINT[1].ENDPT = USB_ENDPT_EPRXEN_MASK | USB_ENDPT_EPHSHK_MASK;
	USB0->ENDPOINT[2].ENDPT = USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPHSHK_MASK;

	USB0->INTEN = 0xFFu;
	USB0->ERREN = 0xFFu;

	USB0->USBCTRL = 0u;
	USB0->CTL = USB_CTL_USBENSOFEN_MASK;

	NVIC_SetPriority(USB0_IRQn, 0u);
	NVIC_EnableIRQ(USB0_IRQn);

	while (1) {
		wdog_refresh();
	}

	return 0;
}
